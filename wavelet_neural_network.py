import time
import numpy as np

from numpy.linalg import norm
from torch.nn.functional import softmax


# 计时装饰器
def timer(func):
    times = []

    def wrapper(*args, **kwargs):
        start_time = time.time()
        func(*args, **kwargs)
        end_time = time.time()
        times.append(end_time - start_time)
        print('\nTraining time is :{0:.4f} ± {1:.4f} s.'.format(np.mean(times), np.std(times)))

    return wrapper


class WaveletNeuralNet(object):

    # 初始化神经网络
    def __init__(self, sizes, x_train):

        self.sizes_ = sizes

        self.x_train_ = x_train

        self.number_of_class_ = sizes[2]

        # 层数
        self.num_layers_ = len(sizes)

        # 权重w_、b_初始化为正态分布随机数
        self.w_ = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]

        # 偏移
        self.b_ = [np.random.randn(y, 1) for y in sizes[1:]]

        # 平移因子
        self.s_ = 0.5 * (np.max(self.x_train_) + np.min(self.x_train_))

        # 伸缩因子
        self.t_ = 0.2 * (np.max(self.x_train_) - np.min(self.x_train_))

    # 标签转化
    def one_hot(self, x, number_of_class):
        x = x.flatten().astype('uint8')
        m = x.shape[0]
        x_onehot = np.zeros((m, number_of_class))
        for i in range(m):
            x_onehot[i, x[i]] = 1
        return x_onehot

    # Sigmoid函数，S型曲线，
    def sigmoid(self, x):
        return 1.0 / (1.0 + np.exp(-x))

    # Sigmoid函数的导函数
    def sigmoid_der(self, x):
        return self.sigmoid(x) * (1 - self.sigmoid(x))

    # Morlet小波母函数
    def morlet(self, z, t=1, s=0):
        x = (z - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return (np.cos(1.75 * x) * c_exp) / t

    # Morlet 小波导函数
    def morlet_der(self, z, t=1, s=0):
        x = (z - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return (-1.75 * np.sin(1.75 * x) * c_exp - x * np.cos(1.75 * x) * c_exp) / t

    # Mexican Hat小波母函数
    def mexican_hat(self, x, t=1, s=0):
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return ((1 - np.power(x, 2)) * c_exp) / t

    # Mexican Hat 小波导函数
    def mexican_hat_der(self, x, t=1, s=0):
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return ((np.power(x, 3) - 3 * x) * c_exp) / t

    # POLYWOG1小波母函数
    def polywog1(self, x, t=1, s=0):
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return ((2 / np.sqrt(np.pi)) * x * c_exp) / t

    #  POLYWOG1 小波导函数
    def polywog1_der(self, x, t=1, s=0):
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return ((2 / np.sqrt(np.pi)) * (1 - np.power(x, 2)) * c_exp) / t

    # POLYWOG2 小波母函数
    def polywog2(self, x, t=1, s=0):
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return ((4 / (3 * np.sqrt(np.pi))) * (1 - np.power(x, 2)) * c_exp) / t

    # POLYWOG2 小波导函数
    def polywog2_der(self, x, t=1, s=0):
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return ((4 / (3 * np.sqrt(np.pi))) * (np.power(x, 3) - 3 * x) * c_exp) / t

    # POLYWOG3 小波母函数
    def polywog3(self, x, t=1, s=0):
        k = 15 / (8 * np.sqrt(np.pi))
        x = (x - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return (k * (np.power(x, 3) - 3 * x) * c_exp) / t

    # POLYWOG3 小波导函数
    def polywog3_der(self, z, t=1, s=0):
        k = 15 / (8 * np.sqrt(np.pi))
        x = (z - s) / t
        c_exp = np.exp(-0.5 * np.power(x, 2))
        return (k * (4 * np.power(x, 2) - np.power(x, 4) - 1) * c_exp) / t

    # 前向
    def feedforward(self, x):
        n = self.w_[0].shape[1]
        x = x.reshape(n, -1)
        x1 = self.mexican_hat(np.dot(self.w_[0], x) + self.b_[0], self.t_, self.s_)
        x2 = self.sigmoid(np.dot(self.w_[1], x1) + self.b_[1])
        return x2

    # 反向传播
    def backprop(self, x, y):

        func = self.mexican_hat
        func_der = self.mexican_hat_der

        b_new = [np.zeros(b.shape) for b in self.b_]
        w_new = [np.zeros(w.shape) for w in self.w_]
        t_new = self.t_
        s_new = self.s_
        activation = x

        # activations代表着每层的输出
        activations = [x]

        # zs代表着每层的输入，即前层输出与权重的和
        zs = []

        z = np.dot(self.w_[0], activation) + self.b_[0]
        zs.append(z)
        activation = func(z, t_new, s_new)
        activations.append(activation)
        z = np.dot(self.w_[1], activation) + self.b_[1]
        zs.append(z)
        activation = self.sigmoid(z)
        activations.append(activation)

        delta = self.cost_derivative(activations[-1], y) * self.sigmoid_der(zs[-1])
        b_new[-1] = delta
        w_new[-1] = np.dot(delta, activations[-2].transpose())

        delta_last = delta.copy()
        z = zs[-2]
        sp = func_der(z, t_new, s_new)
        delta = np.dot(self.w_[-1].transpose(), delta_last) * sp
        b_new[-2] = delta
        w_new[-2] = np.dot(delta, activations[-3].transpose())

        sp_t = -0.5 * t_new ** -1.5 * func((z - s_new) / t_new) - t_new ** -2.5 * (z - s_new) * func_der((z - s_new) / t_new)
        sp_s = -t_new ** -1.5 * func_der((z - s_new) / t_new)

        # # loss函数对小波函数缩放/平移系数的偏导
        t_new = delta * sp_t
        s_new = delta * sp_s

        # t_new = np.dot(self.w_[-1].transpose(), delta_last) * sp_t
        # s_new = np.dot(self.w_[-1].transpose(), delta_last) * sp_s

        return b_new, w_new, t_new, s_new

    # 更新权值w，偏移b，缩放因子t，偏移因子s
    def update_mini_batch(self, mini_batch, lr):
        global t_new, s_new
        b_new = [np.zeros(b.shape) for b in self.b_]
        w_new = [np.zeros(w.shape) for w in self.w_]
        a, b = mini_batch[:, :-1], self.one_hot(mini_batch[:, -1], self.number_of_class_)
        n = np.float(mini_batch.shape[0])
        for i in range(int(n)):
            x, y = a[i, :].reshape(-1, 1), b[i, :].reshape(-1, 1)
            delta_b_new, delta_w_new, t_new, s_new = self.backprop(x, y)
            b_new = [nb + dnb for nb, dnb in zip(b_new, delta_b_new)]
            w_new = [nw + dnw for nw, dnw in zip(w_new, delta_w_new)]
        self.w_ = [w - lr * nw for w, nw in zip(self.w_, w_new)]
        self.b_ = [b - lr * nb for b, nb in zip(self.b_, b_new)]
        self.t_ = self.t_ - lr * t_new
        self.s_ = self.s_ - lr * s_new

    @timer
    def SGD(self, training_data, epochs, mini_batch_size, learning_rate, step):
        n = training_data[0].shape[0]
        pre_loss = []
        pre_accur = []
        repet = 0

        for j in range(epochs):
            ss = np.hstack((training_data[0], training_data[1].reshape(n, -1)))
            np.random.shuffle(ss)
            mini_batches = [ss[k:k + mini_batch_size, :] for k in range(0, n, mini_batch_size)]
            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, learning_rate)
            accur = self.evaluate(training_data)
            mse_loss = self.mse_loss(training_data)
            pre_loss.append(mse_loss)
            pre_accur.append(accur)

            if (j + 1) % step == 0 or j == 0:
                print("Epochs: {0},  mse_loss: {1:.4f},  training_accuracy: {2:.4f}%".format(j + 1, mse_loss, accur * 100))
            if j > 1:
                if (pre_loss[-2] - mse_loss) < 1e-8 and (accur - pre_accur[-2]) < 1e-8:
                    repet += 1
                    if repet == 2:
                        break

    # 计算正确率
    def evaluate(self, data):
        x_t, x_label = data
        test_results = [(np.argmax(self.feedforward(x)), y) for (x, y) in zip(list(x_t), list(x_label))]
        acc = sum(int(x == y) for (x, y) in test_results) / x_t.shape[0]
        return acc

    # mse_loss的导数
    def cost_derivative(self, output_activations, y):
        return (output_activations - y)

    # mse_loss
    def mse_loss(self, training_data):
        x_t, x_label = training_data
        test_results = [0.5 * norm(self.feedforward(x).flatten() - self.one_hot(y, self.number_of_class_)) ** 2 for (x, y) in zip(list(
            x_t), list(x_label))]
        return np.array(test_results).mean()

    # 预测
    def predict(self, data, wnn):
        data = data.reshape(-1, self.sizes_[0])
        score = np.array([wnn.feedforward(x) for x in data])
        value = np.array([np.argmax(wnn.feedforward(x)) for x in data], dtype='uint8')
        return score, value
