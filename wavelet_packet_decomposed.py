import pywt
import time
import numpy as np
from scipy import stats


def extracted_features_timer(func):
    def wrapper(*args, **kwargs):
        times = []
        start_time = time.time()
        func(*args, **kwargs)
        end_time = time.time()
        times.append(end_time - start_time)
        print('\nFeatures extracted time is :{0:.4f}s.\n'.format(np.mean(times)))

    return wrapper


# 小波包分解提取特征
def wavelet_packet_decomposition(signal, i, wavalet_name, level, extracted_features):
    wp = pywt.WaveletPacket(signal, wavalet_name, mode='symmetric', maxlevel=level)
    # 各子波段的平均值
    extracted_features[i, 0] = np.mean(abs(wp['a'].data))
    extracted_features[i, 1] = np.mean(abs(wp['aa'].data))
    extracted_features[i, 2] = np.mean(abs(wp['aaa'].data))
    extracted_features[i, 3] = np.mean(abs(wp['aaaa'].data))
    extracted_features[i, 4] = np.mean(abs(wp['aaaaa'].data))
    extracted_features[i, 5] = np.mean(abs(wp['aaaaaa'].data))
    extracted_features[i, 6] = np.mean(abs(wp['d'].data))
    extracted_features[i, 7] = np.mean(abs(wp['dd'].data))
    extracted_features[i, 8] = np.mean(abs(wp['ddd'].data))
    extracted_features[i, 9] = np.mean(abs(wp['dddd'].data))
    extracted_features[i, 10] = np.mean(abs(wp['ddddd'].data))
    extracted_features[i, 11] = np.mean(abs(wp['dddddd'].data))

    # 各子波段的标准偏差
    extracted_features[i, 12] = np.std(wp['a'].data)
    extracted_features[i, 13] = np.std(wp['aa'].data)
    extracted_features[i, 14] = np.std(wp['aaa'].data)
    extracted_features[i, 15] = np.std(wp['aaaa'].data)
    extracted_features[i, 16] = np.std(wp['aaaaa'].data)
    extracted_features[i, 17] = np.std(wp['aaaaaa'].data)
    extracted_features[i, 18] = np.std(wp['d'].data)
    extracted_features[i, 19] = np.std(wp['dd'].data)
    extracted_features[i, 20] = np.std(wp['ddd'].data)
    extracted_features[i, 21] = np.std(wp['dddd'].data)
    extracted_features[i, 22] = np.std(wp['ddddd'].data)
    extracted_features[i, 23] = np.std(wp['dddddd'].data)

    # 各子波段的中值
    extracted_features[i, 24] = np.median(wp['a'].data)
    extracted_features[i, 25] = np.median(wp['aa'].data)
    extracted_features[i, 26] = np.median(wp['aaa'].data)
    extracted_features[i, 27] = np.median(wp['aaaa'].data)
    extracted_features[i, 28] = np.median(wp['aaaaa'].data)
    extracted_features[i, 29] = np.median(wp['aaaaaa'].data)
    extracted_features[i, 30] = np.median(wp['d'].data)
    extracted_features[i, 31] = np.median(wp['dd'].data)
    extracted_features[i, 32] = np.median(wp['ddd'].data)
    extracted_features[i, 33] = np.median(wp['dddd'].data)
    extracted_features[i, 34] = np.median(wp['ddddd'].data)
    extracted_features[i, 35] = np.median(wp['dddddd'].data)

    # 各子带的偏斜度
    extracted_features[i, 36] = stats.skew(wp['a'].data)
    extracted_features[i, 37] = stats.skew(wp['aa'].data)
    extracted_features[i, 38] = stats.skew(wp['aaa'].data)
    extracted_features[i, 39] = stats.skew(wp['aaaa'].data)
    extracted_features[i, 40] = stats.skew(wp['aaaaa'].data)
    extracted_features[i, 41] = stats.skew(wp['aaaaaa'].data)
    extracted_features[i, 42] = stats.skew(wp['d'].data)
    extracted_features[i, 43] = stats.skew(wp['dd'].data)
    extracted_features[i, 44] = stats.skew(wp['ddd'].data)
    extracted_features[i, 45] = stats.skew(wp['dddd'].data)
    extracted_features[i, 46] = stats.skew(wp['ddddd'].data)
    extracted_features[i, 47] = stats.skew(wp['dddddd'].data)

    # 各子带的峰度
    extracted_features[i, 48] = stats.kurtosis(wp['a'].data)
    extracted_features[i, 49] = stats.kurtosis(wp['aa'].data)
    extracted_features[i, 50] = stats.kurtosis(wp['aaa'].data)
    extracted_features[i, 51] = stats.kurtosis(wp['aaaa'].data)
    extracted_features[i, 52] = stats.kurtosis(wp['aaaaa'].data)
    extracted_features[i, 53] = stats.kurtosis(wp['aaaaaa'].data)
    extracted_features[i, 54] = stats.kurtosis(wp['d'].data)
    extracted_features[i, 55] = stats.kurtosis(wp['dd'].data)
    extracted_features[i, 56] = stats.kurtosis(wp['ddd'].data)
    extracted_features[i, 57] = stats.kurtosis(wp['dddd'].data)
    extracted_features[i, 58] = stats.kurtosis(wp['ddddd'].data)
    extracted_features[i, 59] = stats.kurtosis(wp['dddddd'].data)

    # 各子波段的均方根值
    extracted_features[i, 60] = np.sqrt(np.mean(wp['a'].data ** 2))
    extracted_features[i, 61] = np.sqrt(np.mean(wp['aa'].data ** 2))
    extracted_features[i, 62] = np.sqrt(np.mean(wp['aaa'].data ** 2))
    extracted_features[i, 63] = np.sqrt(np.mean(wp['aaaa'].data ** 2))
    extracted_features[i, 64] = np.sqrt(np.mean(wp['aaaaa'].data ** 2))
    extracted_features[i, 65] = np.sqrt(np.mean(wp['aaaaaa'].data ** 2))
    extracted_features[i, 66] = np.sqrt(np.mean(wp['d'].data ** 2))
    extracted_features[i, 67] = np.sqrt(np.mean(wp['dd'].data ** 2))
    extracted_features[i, 68] = np.sqrt(np.mean(wp['ddd'].data ** 2))
    extracted_features[i, 69] = np.sqrt(np.mean(wp['dddd'].data ** 2))
    extracted_features[i, 70] = np.sqrt(np.mean(wp['ddddd'].data ** 2))
    extracted_features[i, 71] = np.sqrt(np.mean(wp['dddddd'].data ** 2))

    # 子带比
    extracted_features[i, 72] = np.mean(abs(wp['a'].data)) / np.mean(abs(wp['aa'].data))
    extracted_features[i, 73] = np.mean(abs(wp['aa'].data)) / np.mean(abs(wp['aaa'].data))
    extracted_features[i, 74] = np.mean(abs(wp['aaa'].data)) / np.mean(abs(wp['aaaa'].data))
    extracted_features[i, 75] = np.mean(abs(wp['aaaa'].data)) / np.mean(abs(wp['aaaaa'].data))
    extracted_features[i, 76] = np.mean(abs(wp['aaaaa'].data)) / np.mean(abs(wp['aaaaaa'].data))
    extracted_features[i, 77] = np.mean(abs(wp['aaaaaa'].data)) / np.mean(abs(wp['d'].data))
    extracted_features[i, 78] = np.mean(abs(wp['d'].data)) / np.mean(abs(wp['dd'].data))
    extracted_features[i, 79] = np.mean(abs(wp['dd'].data)) / np.mean(abs(wp['ddd'].data))
    extracted_features[i, 80] = np.mean(abs(wp['ddd'].data)) / np.mean(abs(wp['dddd'].data))
    extracted_features[i, 81] = np.mean(abs(wp['dddd'].data)) / np.mean(abs(wp['ddddd'].data))
    extracted_features[i, 82] = np.mean(abs(wp['ddddd'].data)) / np.mean(abs(wp['dddddd'].data))

    # 各子波段的极差
    extracted_features[i, 83] = np.ptp(wp['a'].data)
    extracted_features[i, 84] = np.ptp(wp['aa'].data)
    extracted_features[i, 85] = np.ptp(wp['aaa'].data)
    extracted_features[i, 86] = np.ptp(wp['aaaa'].data)
    extracted_features[i, 87] = np.ptp(wp['aaaaa'].data)
    extracted_features[i, 88] = np.ptp(wp['aaaaaa'].data)
    extracted_features[i, 89] = np.ptp(wp['d'].data)
    extracted_features[i, 90] = np.ptp(wp['dd'].data)
    extracted_features[i, 91] = np.ptp(wp['ddd'].data)
    extracted_features[i, 92] = np.ptp(wp['dddd'].data)
    extracted_features[i, 93] = np.ptp(wp['ddddd'].data)
    extracted_features[i, 94] = np.ptp(wp['dddddd'].data)

    # # 各子波段的能量
    # extracted_features[i, 95] = pow(np.linalg.norm(wp['a'].data), 2)
    # extracted_features[i, 96] = pow(np.linalg.norm(wp['aa'].data), 2)
    # extracted_features[i, 97] = pow(np.linalg.norm(wp['aaa'].data), 2)
    # extracted_features[i, 98] = pow(np.linalg.norm(wp['aaaa'].data), 2)
    # extracted_features[i, 99] = pow(np.linalg.norm(wp['aaaaa'].data), 2)
    # extracted_features[i, 100] = pow(np.linalg.norm(wp['aaaaaa'].data), 2)
    # extracted_features[i, 101] = pow(np.linalg.norm(wp['d'].data), 2)
    # extracted_features[i, 102] = pow(np.linalg.norm(wp['dd'].data), 2)
    # extracted_features[i, 103] = pow(np.linalg.norm(wp['ddd'].data), 2)
    # extracted_features[i, 104] = pow(np.linalg.norm(wp['dddd'].data), 2)
    # extracted_features[i, 105] = pow(np.linalg.norm(wp['ddddd'].data), 2)
    # extracted_features[i, 106] = pow(np.linalg.norm(wp['dddddd'].data), 2)

    # extracted_features[i, 0] = pow(np.linalg.norm(wp['aaaaad'].data, ord=None), 2)
    # extracted_features[i, 1] = pow(np.linalg.norm(wp['aaaadd'].data, ord=None), 2)
    # extracted_features[i, 2] = np.mean(abs(wp['a'].data))
    # extracted_features[i, 3] = np.mean(abs(wp['aa'].data))
    # extracted_features[i, 4] = np.mean(abs(wp['aaa'].data))
    # extracted_features[i, 5] = np.mean(abs(wp['d'].data))
    # extracted_features[i, 6] = np.mean(abs(wp['dd'].data))
    # extracted_features[i, 7] = np.mean(abs(wp['ddd'].data))
    # extracted_features[i, 8] = np.std(wp['a'].data)
    # extracted_features[i, 9] = np.std(wp['aa'].data)
    # extracted_features[i, 10] = np.std(wp['aaa'].data)
    # extracted_features[i, 11] = np.std(wp['d'].data)
    # extracted_features[i, 12] = np.std(wp['dd'].data)
    # extracted_features[i, 13] = np.std(wp['ddd'].data)
    # extracted_features[i, 14] = np.median(wp['a'].data)
    # extracted_features[i, 15] = np.median(wp['aa'].data)
    # extracted_features[i, 16] = np.median(wp['aaa'].data)
    # extracted_features[i, 17] = np.median(wp['d'].data)
    # extracted_features[i, 18] = np.median(wp['dd'].data)
    # extracted_features[i, 19] = np.median(wp['ddd'].data)
    # extracted_features[i, 20] = stats.skew(wp['a'].data)
    # extracted_features[i, 21] = stats.skew(wp['aa'].data)
    # extracted_features[i, 22] = stats.skew(wp['aaa'].data)
    # extracted_features[i, 23] = stats.skew(wp['d'].data)
    # extracted_features[i, 24] = stats.skew(wp['dd'].data)
    # extracted_features[i, 25] = stats.skew(wp['ddd'].data)
    # extracted_features[i, 26] = stats.kurtosis(wp['a'].data)
    # extracted_features[i, 27] = stats.kurtosis(wp['aa'].data)
    # extracted_features[i, 28] = stats.kurtosis(wp['aaa'].data)
    # extracted_features[i, 29] = stats.kurtosis(wp['d'].data)
    # extracted_features[i, 30] = stats.kurtosis(wp['dd'].data)
    # extracted_features[i, 31] = stats.kurtosis(wp['ddd'].data)
    # extracted_features[i, 32] = np.sqrt(np.mean(wp['a'].data ** 2))
    # extracted_features[i, 33] = np.sqrt(np.mean(wp['aa'].data ** 2))
    # extracted_features[i, 34] = np.sqrt(np.mean(wp['aaa'].data ** 2))
    # extracted_features[i, 35] = np.sqrt(np.mean(wp['d'].data ** 2))
    # extracted_features[i, 36] = np.sqrt(np.mean(wp['dd'].data ** 2))
    # extracted_features[i, 37] = np.sqrt(np.mean(wp['ddd'].data ** 2))
    # extracted_features[i, 38] = np.mean(abs(wp['a'].data)) / np.mean(abs(wp['aa'].data))
    # extracted_features[i, 39] = np.mean(abs(wp['aa'].data)) / np.mean(abs(wp['aaa'].data))
    # extracted_features[i, 40] = np.mean(abs(wp['d'].data)) / np.mean(abs(wp['dd'].data))
    # extracted_features[i, 41] = np.mean(abs(wp['dd'].data)) / np.mean(abs(wp['ddd'].data))
    # extracted_features[i, 42] = np.ptp(wp['a'].data)
    # extracted_features[i, 43] = np.ptp(wp['aa'].data)
    # extracted_features[i, 44] = np.ptp(wp['aaa'].data)
    # extracted_features[i, 45] = np.ptp(wp['d'].data)
    # extracted_features[i, 46] = np.ptp(wp['dd'].data)
    # extracted_features[i, 47] = np.ptp(wp['ddd'].data)
    # extracted_features[i, 48] = pow(np.linalg.norm(wp['a'].data), 2)
    # extracted_features[i, 49] = pow(np.linalg.norm(wp['aa'].data), 2)
    # extracted_features[i, 50] = pow(np.linalg.norm(wp['aaa'].data), 2)
    # extracted_features[i, 51] = pow(np.linalg.norm(wp['d'].data), 2)
    # extracted_features[i, 52] = pow(np.linalg.norm(wp['dd'].data), 2)
    # extracted_features[i, 53] = pow(np.linalg.norm(wp['ddd'].data), 2)

    # extracted_features[i, 0] = pow(np.linalg.norm(wp['a'].data, ord=None), 2)
    # extracted_features[i, 1] = pow(np.linalg.norm(wp['aa'].data, ord=None), 2)
    # extracted_features[i, 2] = pow(np.linalg.norm(wp['aaa'].data, ord=None), 2)
    # extracted_features[i, 3] = pow(np.linalg.norm(wp['aaaa'].data, ord=None), 2)
    # extracted_features[i, 4] = pow(np.linalg.norm(wp['aaaaa'].data, ord=None), 2)
    # extracted_features[i, 5] = pow(np.linalg.norm(wp['aaaaaa'].data, ord=None), 2)
    # extracted_features[i, 6] = pow(np.linalg.norm(wp['d'].data, ord=None), 2)
    # extracted_features[i, 7] = pow(np.linalg.norm(wp['dd'].data, ord=None), 2)
    # extracted_features[i, 8] = pow(np.linalg.norm(wp['ddd'].data, ord=None), 2)
    # extracted_features[i, 9] = pow(np.linalg.norm(wp['dddd'].data, ord=None), 2)
    # extracted_features[i, 10] = pow(np.linalg.norm(wp['ddddd'].data, ord=None), 2)
    # extracted_features[i, 11] = pow(np.linalg.norm(wp['dddddd'].data, ord=None), 2)


# 构建特征向量
@extracted_features_timer
def wpd_feature_array(level, each_class_number_of_signal, wavelet_name, class1, class2, class3, class4, class5, class6, features, labels):
    print('Extracted features for class1...')
    for i in range(each_class_number_of_signal):
        wavelet_packet_decomposition(class1[i, :], i, wavelet_name, level, features)
        labels.append(0)

    print('Extracted features for class2...')
    for i in range(each_class_number_of_signal, 2 * each_class_number_of_signal):
        wavelet_packet_decomposition(class2[i - each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(1)

    print('Extracted features for class3...')
    for i in range(2 * each_class_number_of_signal, 3 * each_class_number_of_signal):
        wavelet_packet_decomposition(class3[i - 2 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(2)

    print('Extracted features for class4...')
    for i in range(3 * each_class_number_of_signal, 4 * each_class_number_of_signal):
        wavelet_packet_decomposition(class4[i - 3 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(3)

    print('Extracted features for class5...')
    for i in range(4 * each_class_number_of_signal, 5 * each_class_number_of_signal):
        wavelet_packet_decomposition(class5[i - 4 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(4)

    print('Extracted features for class6...')
    for i in range(5 * each_class_number_of_signal, 6 * each_class_number_of_signal):
        wavelet_packet_decomposition(class6[i - 5 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(5)
