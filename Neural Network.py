# import torch
# import torch.nn as nn
# import numpy as np
#
#
# class Net(nn.Module):
#     def __init__(self, n_feature, n_hidden, n_output):
#         super(Net, self).__init__()
#         self.hidden = nn.Linear(n_feature, n_hidden)
#         self.predict = nn.Linear(n_hidden, n_output)
#
#     def forward(self, x):
#         x = torch.sigmoid(self.hidden(x))
#         x = torch.softmax(self.predict(x))
#         return x
#
#     def update_mini_batch(self, loss_func, optimizer, training_data):
#         optimizer.zero_grad()
#         for i, (x, y) in enumerate(training_data):
#             x = np.array(x)
#             x = torch.from_numpy(x.T)
#             y = np.array(y)
#             y = np.nanargmax(x)
#             y = torch.tensor([y])
#             prediction = self(x)
#             loss = loss_func(prediction, y)
#         loss.backward()
#         optimizer.setp()
#
#     def train(self, training_data, testing_data, epochs, mini_batch_size, learning_rate):
#         optimizer = torch.optim.adam(self.parameters(), lr=learning_rate)
#         loss_func = nn.CrossEntropyLoss()
#         for j in range(epochs):
#             np.random.shuffle(training_data)
#             mini_batches = [training_data[k:k + mini_batch] for k in range(0, len(training_data), mini_batch_size)]
#             for mini_batch in mini_batches:
#                 self.update_mini_batch(loss_func, optimizer, mini_batch)
#
#             result = 0
#             for test_x, test_y in test_data:
#                 test_x = np.array(test_x)
#                 test_x = torch.from_numpy(test_x.T)
#                 test_prediction = self(test_x)
#                 test_prediction = test_prediction.data.numpy()
#                 if np.nanargmax(test_prediction) == test_y:
#                     result = result + 1
#             print("Epoch {}: {} / {}".format(j, result, len(test_data)))
#
#
# net = Net(784, 30, 10)
# training_data, validation_data, test_data = 0
# training_data = list(training_data)
# test_data = list(test_data)
# n_test = len(test_data)
#
# net.train((training_data, test_data, 100, 10, 0.25))
import pywt

x = [1, 2, 3, 4, 5, 6, 77, 8, 1, 1, 1, 1, 1, 1, 11, 1, 1]
wp = pywt.WaveletPacket(x, wavelet='db2', mode='symmetric', maxlevel=3)
print([node.path for node in wp.get_level(1, 'freq')])
print([node.path for node in wp.get_level(2, 'freq')])
print([node.path for node in wp.get_level(3, 'freq')])
