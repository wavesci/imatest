import pywt
import time
import numpy as np
from scipy import stats


def extracted_features_timer(func):
    def wrapper(*args, **kwargs):
        times = []
        start_time = time.time()
        func(*args, **kwargs)
        end_time = time.time()
        times.append(end_time - start_time)
        print('\nFeatures extracted time is :{0:.4f}s.\n'.format(np.mean(times)))

    return wrapper


# 小波包分解提取特征
def discrete_wavelet_transformation(signal, i, wavalet_name, level, extracted_features):
    coeff = pywt.wavedec(signal, wavalet_name, level=level)
    cA6, cD6, cD5, cD4, cD3, cD2, cD1 = coeff
    # 各子波段的平均值
    extracted_features[i, 0] = np.mean(abs(cD1[:]))
    extracted_features[i, 1] = np.mean(abs(cD2[:]))
    extracted_features[i, 2] = np.mean(abs(cD3[:]))
    extracted_features[i, 3] = np.mean(abs(cD4[:]))
    extracted_features[i, 4] = np.mean(abs(cD5[:]))
    extracted_features[i, 5] = np.mean(abs(cD6[:]))
    extracted_features[i, 6] = np.mean(abs(cA6[:]))

    # 各子波段的标准偏差
    extracted_features[i, 7] = np.std(abs(cD1[:]))
    extracted_features[i, 8] = np.std(abs(cD2[:]))
    extracted_features[i, 9] = np.std(abs(cD3[:]))
    extracted_features[i, 10] = np.std(abs(cD4[:]))
    extracted_features[i, 11] = np.std(abs(cD5[:]))
    extracted_features[i, 12] = np.std(abs(cD6[:]))
    extracted_features[i, 13] = np.std(abs(cA6[:]))

    # 各子波段的中值
    extracted_features[i, 14] = np.median(cD1[:])
    extracted_features[i, 15] = np.median(cD2[:])
    extracted_features[i, 16] = np.median(cD3[:])
    extracted_features[i, 17] = np.median(cD4[:])
    extracted_features[i, 18] = np.median(cD5[:])
    extracted_features[i, 19] = np.median(cD6[:])
    extracted_features[i, 20] = np.median(cA6[:])

    # 各子带的偏斜度
    extracted_features[i, 21] = stats.skew(cD1[:])
    extracted_features[i, 22] = stats.skew(cD2[:])
    extracted_features[i, 23] = stats.skew(cD3[:])
    extracted_features[i, 24] = stats.skew(cD4[:])
    extracted_features[i, 25] = stats.skew(cD5[:])
    extracted_features[i, 26] = stats.skew(cD6[:])
    extracted_features[i, 27] = stats.skew(cA6[:])

    # 各子带的峰度
    extracted_features[i, 28] = stats.kurtosis(cD1[:])
    extracted_features[i, 29] = stats.kurtosis(cD2[:])
    extracted_features[i, 30] = stats.kurtosis(cD3[:])
    extracted_features[i, 31] = stats.kurtosis(cD4[:])
    extracted_features[i, 32] = stats.kurtosis(cD5[:])
    extracted_features[i, 33] = stats.kurtosis(cD6[:])
    extracted_features[i, 34] = stats.kurtosis(cA6[:])

    # 各子波段的均方根值
    extracted_features[i, 35] = np.sqrt(np.mean(cD1[:] ** 2))
    extracted_features[i, 36] = np.sqrt(np.mean(cD2[:] ** 2))
    extracted_features[i, 37] = np.sqrt(np.mean(cD3[:] ** 2))
    extracted_features[i, 38] = np.sqrt(np.mean(cD4[:] ** 2))
    extracted_features[i, 39] = np.sqrt(np.mean(cD5[:] ** 2))
    extracted_features[i, 40] = np.sqrt(np.mean(cD6[:] ** 2))
    extracted_features[i, 41] = np.sqrt(np.mean(cA6[:] ** 2))

    # 子带比
    extracted_features[i, 42] = np.mean(abs(cD1[:])) / np.mean(abs(cD2[:]))
    extracted_features[i, 43] = np.mean(abs(cD2[:])) / np.mean(abs(cD3[:]))
    extracted_features[i, 44] = np.mean(abs(cD3[:])) / np.mean(abs(cD4[:]))
    extracted_features[i, 45] = np.mean(abs(cD4[:])) / np.mean(abs(cD5[:]))
    extracted_features[i, 46] = np.mean(abs(cD5[:])) / np.mean(abs(cD6[:]))
    extracted_features[i, 47] = np.mean(abs(cD6[:])) / np.mean(abs(cA6[:]))

    # 各子波段的极差
    extracted_features[i, 48] = np.ptp(cD1[:])
    extracted_features[i, 49] = np.ptp(cD2[:])
    extracted_features[i, 50] = np.ptp(cD3[:])
    extracted_features[i, 51] = np.ptp(cD4[:])
    extracted_features[i, 52] = np.ptp(cD5[:])
    extracted_features[i, 53] = np.ptp(cD6[:])
    extracted_features[i, 54] = np.ptp(cA6[:])


# 构建特征向量
@extracted_features_timer
def dwt_feature_array(level, each_class_number_of_signal, wavelet_name, class1, class2, class3, class4, class5, class6, features, labels):

    print('Extracted features for class1...')
    for i in range(each_class_number_of_signal):
        discrete_wavelet_transformation(class1[i, :], i, wavelet_name, level, features)
        labels.append(0)

    print('Extracted features for class2...')
    for i in range(each_class_number_of_signal, 2 * each_class_number_of_signal):
        discrete_wavelet_transformation(class2[i - each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(1)

    print('Extracted features for class3...')
    for i in range(2 * each_class_number_of_signal, 3 * each_class_number_of_signal):
        discrete_wavelet_transformation(class3[i - 2 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(2)

    print('Extracted features for class4...')
    for i in range(3 * each_class_number_of_signal, 4 * each_class_number_of_signal):
        discrete_wavelet_transformation(class4[i - 3 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(3)

    print('Extracted features for class5...')
    for i in range(4 * each_class_number_of_signal, 5 * each_class_number_of_signal):
        discrete_wavelet_transformation(class5[i - 4 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(4)

    print('Extracted features for class6...')
    for i in range(5 * each_class_number_of_signal, 6 * each_class_number_of_signal):
        discrete_wavelet_transformation(class6[i - 5 * each_class_number_of_signal, :], i, wavelet_name, level, features)
        labels.append(5)
