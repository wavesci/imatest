import mne
import warnings
import numpy as np

# 去掉警告
warnings.simplefilter("ignore", FutureWarning)
warnings.simplefilter("ignore", RuntimeWarning)
warnings.simplefilter("ignore", DeprecationWarning)


def preprocessing(filename, class1, class2, class3, class4, class5, class6):
    # 读入数据
    raw = mne.io.read_raw_eeglab(filename, preload=True)

    # # 5
    # drop_channel_name = ['F3', 'F1', 'Fz', 'F2', 'F4', 'FFC5h', 'FFC3h', 'FFC1h', 'FFC2h', 'FFC4h', 'FFC6h', 'FC5', 'FC3', 'FC1',
    #                      'FC2', 'FC4', 'FC6', 'FTT7h', 'FCC5h', 'FCC3h', 'FCC1h', 'FCC2h', 'FCC4h', 'FCC6h', 'FTT8h', 'C5',
    #                      'C1', 'C2', 'C6', 'TTP7h', 'CCP5h', 'CCP3h', 'CCP1h', 'CCP2h', 'CCP4h', 'CCP6h', 'TTP8h', 'CP5',
    #                      'CP3', 'CP1', 'CP2', 'CP4', 'CP6', 'CPP5h', 'CPP3h', 'CPP1h', 'CPP2h', 'CPP4h', 'CPP6h', 'P3', 'P1',
    #                      'Pz', 'P2', 'P4', 'PPO1h', 'PPO2h']
    # 15
    # drop_channel_name = ['F3', 'F1', 'Fz', 'F2', 'F4', 'FFC5h', 'FFC3h', 'FFC1h', 'FFC2h', 'FFC4h', 'FFC6h', 'FC5', 'FC3', 'FC1',
    #                      'FC2', 'FC4', 'FC6', 'FTT7h', 'FCC5h', 'FCC6h', 'FTT8h', 'C5', 'C6', 'TTP7h', 'CCP5h', 'CCP6h', 'TTP8h', 'CP5',
    #                      'CP3', 'CP1', 'CP2', 'CP4', 'CP6', 'CPP5h', 'CPP3h', 'CPP1h', 'CPP2h', 'CPP4h', 'CPP6h', 'P3', 'P1',
    #                      'Pz', 'P2', 'P4', 'PPO1h', 'PPO2h']

    # # 27
    # drop_channel_name = ['F3', 'F1', 'Fz', 'F2', 'F4', 'FFC5h', 'FFC3h', 'FFC1h', 'FFC2h', 'FFC4h', 'FFC6h', 'FC5', 'FC6', 'FTT7h',
    #                      'FTT8h', 'C5', 'C6', 'TTP7h', 'TTP8h', 'CP5', 'CP6', 'CPP5h', 'CPP3h', 'CPP1h', 'CPP2h', 'CPP4h', 'CPP6h',
    #                      'P3', 'P1', 'Pz', 'P2', 'P4', 'PPO1h', 'PPO2h']

    # # 41
    # drop_channel_name = ['F3', 'F1', 'F2', 'F4', 'FFC5h', 'FFC6h', 'FC5', 'FC6', 'FTT7h', 'FTT8h', 'TTP7h', 'TTP8h', 'CP5',
    #                      'CP6', 'CPP5h', 'CPP6h', 'P3', 'P4', 'PPO1h', 'PPO2h']
    # raw.drop_channels(drop_channel_name)

    # 标记坏的通道
    # raw.info['bads'] = ['C5', 'CP6', 'FTT7h']

    # 电极定位
    # montage = mne.channels.make_standard_montage('standard_1005')
    # raw.set_montage(montage)

    # 插补坏导
    # raw = raw.interpolate_bads()

    # 重参考
    # raw_data.set_eeg_reference('average')

    # 滤波
    # raw.notch_filter(50.)
    raw.filter(8, 30)

    # 重采样
    # raw.resample(sfreq=64, npad='auto')

    # 读取事件
    events, _ = mne.events_from_annotations(raw)
    event_id = {'1536': 1, '1537': 2, '1538': 3, '1539': 4, '1540': 5, '1541': 6}

    # 数据分段
    epoch = mne.Epochs(raw, events, event_id=event_id, tmin=-0.5, tmax=3.0, baseline=None, preload=True)

    EF = epoch['1536'].get_data()
    EE = epoch['1537'].get_data()
    FP = epoch['1538'].get_data()
    FS = epoch['1539'].get_data()
    HO = epoch['1540'].get_data()
    HC = epoch['1541'].get_data()

    # 释放内存
    del raw
    del epoch

    # 存入数据
    for i in range(0, 12):
        for j in range(0, 61):
            class1.append(EF[i][j])
            class2.append(EE[i][j])
            class3.append(FP[i][j])
            class4.append(FS[i][j])
            class5.append(HO[i][j])
            class6.append(HC[i][j])


def one_hot(x, number_of_class):
    x = x.flatten().astype('uint8')
    m = x.shape[0]
    x_onehot = np.zeros((m, number_of_class))
    for i in range(m):
        x_onehot[i, x[i]] = 1
    return x_onehot
