import torch

x = torch.empty(5, 3)
print(x)

# import numpy as np
# import torch.nn as nn
#
# torch.manual_seed(420)
# x = torch.rand((500, 20), dtype=torch.float32) * 100
# y = torch.randint(low=0, high=5, size=(500,), dtype=torch.float32)
# input_ = x.shape[1]
# output_ = len(y.unique())
#
#
# class Model(nn.Module):
#     def __init__(self, in_features=40, out_features=2):
#         super(Model, self).__init__()
#         self.hidden_layer1 = nn.Linear(in_features, 13, bias=True)
#         self.hidden_layer2 = nn.Linear(13, 8, bias=True)
#         self.output_layer = nn.Linear(8, out_features, bias=True)
#
#     def forward(self, x):
#         sigma1 = torch.relu_(self.hidden_layer1(x))
#         sigma2 = torch.relu_(self.hidden_layer2(sigma1))
#         y_pred = self.output_layer(sigma2)
#         return y_pred
#
#
# net = Model(in_features=input_, out_features=output_)
# criterion = nn.CrossEntropyLoss()
#
# optimizer = torch.optim.Adam(Model().parameters(), lr=0.1)
# for epoch in range(100):
#     zhat = net.forward(x)
#     loss = criterion(zhat, y.long())
#     print(epoch, loss)
#     optimizer.zero_grad()
#     loss.backward()
#     optimizer.step()
