# Imatest使用总结



## 1 耀斑测试（Flare）

### 1.1 测试原理
镜头耀斑（Lens Flare）被称为面纱炫光（Veiling Glare），由镜头组件表明与镜头内镜筒之间的反射引起，是一种会使图像模糊的杂散光。它会发生在包括人眼在内的各种光学系统中，也是限制相机模组动态范围（Dynamic Range）的主要因素，尤其是传感器（Sensor）动态范围大于 120dB 或者更大的高动态（HDR）传感器。耀斑只能通过线性RAW图像可靠的测出，转换为 16bit 或者 48bit 文件，Gamma = 1。其计算原理如下：
$$
Flare=V=L(black_{region})/L(white_{region})
$$
式中， _L_ 是传感照度（illuminance）

### 1.2 测试工具

耀斑测试需要的工具如下：

- 耀斑测试卡（ISO-18844:Flare Test Charts）：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/191118_628bf02f_7472009.png "ISO-18844-Flare Test Charts.png")

- 光源工具箱（LED Light Sources）：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/191205_6c4af157_7472009.jpeg "Led Light Sources.jpg")

### 1.3 分析流程



- 使用Imatest均匀性模块（Uniformity）入图片，选感兴趣区域（ROI）
- 选择ISO 18844 flare method C: linearize with gamma或者ISO 18844 flare method C: linearize with Color space（虽然标准要求，但一般不准确）；
- 要测得准确的耀斑值，需要在深阴影中准确测量Gamma。Gamma是由颜色空间指定，但是大多相机并没有根据规范对图像进行编码。因此，深阴影区的Gamma不能从浅灰色区域的Gamma外推。所以，建议使用直接从原始文件转换Gamma = 1的图像来测量耀斑，不建议使用直接从相机获取的JPEG图像测量耀斑；
- 光陷（Black Hold）检测模式建议使用Auto Black hold detection比手动设置的全局阈值检测更容易获得准确值，选择:fa-check-square-o: Flare plot，得出测试结果。



## 2 清晰度测试（Sharpness）

### 2.1 测试原理


### 2.2 测试工具

耀斑测试需要的工具如下：

- 扩展eSFR ISO测试卡（ISO-12233:Extended eSFR ISO test chart）：
![输入图片说明](Extended%20eSFR%20ISO%20test%20chart.png)


### 2.3 分析流程




