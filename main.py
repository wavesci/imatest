import pywt
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# 支持向量机
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier as knn
from wavelet_neural_network import WaveletNeuralNet as wnn
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as lda
from back_propagation_neural_network import BackPropagationNeuralNet as bpnn

from itertools import cycle
from sklearn.decomposition import PCA
from sklearn.preprocessing import StgitandardScaler
from signal_preprocessing import preprocessing, one_hot
from wavelet_packet_decomposed import wpd_feature_array
from discrete_wavelet_transformation import dwt_feature_array

from sklearn.metrics import accuracy_score, cohen_kappa_score, precision_score, f1_score, recall_score
from sklearn.model_selection import train_test_split, ShuffleSplit
from sklearn.metrics import roc_auc_score, average_precision_score, confusion_matrix, roc_curve, auc, precision_recall_curve

if __name__ == '__main__':
    # 数据预处理
    filename = 'Data/P15/ME/ME.set'
    class1, class2, class3, class4, class5, class6 = [], [], [], [], [], []
    preprocessing(filename, class1, class2, class3, class4, class5, class6)
    class1 = np.array(class1)
    class2 = np.array(class2)
    class3 = np.array(class3)
    class4 = np.array(class4)
    class5 = np.array(class5)
    class6 = np.array(class6)

    # 特征构建
    level = 6
    feature_labels = []
    number_of_class = 6
    rows_of_feature = 95
    each_class_number_of_signal = 732
    wavelet_name = pywt.Wavelet('db2')
    features = np.ndarray(shape=(number_of_class * each_class_number_of_signal, rows_of_feature), dtype=float, order='F')
    wpd_feature_array(level, each_class_number_of_signal, wavelet_name, class1, class2, class3, class4, class5, class6, features, feature_labels)

    y = np.array(feature_labels)
    x = np.array(features)
    # np.savetxt('x03.txt', x)
    # np.savetxt('y03.txt', y)

    # 数据集划分
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)

    # 特征标准化
    scaler = StandardScaler()
    x_train = scaler.fit_transform(x_train)
    x_test = scaler.transform(x_test)

    # 特征选择
    pca = PCA(n_components=0.99)
    x_train = pca.fit_transform(x_train)
    x_test = pca.transform(x_test)
    training_data, testing_data = (x_train, y_train), (x_test, y_test)

    # 设置网络参数
    step = 1
    epochs = 256
    mini_batch_size = 16
    learning_rate = 1e-1
    node_size = x_train.shape[1]

    # 训练小波神经网络分类器
    wnn = wnn([node_size, node_size * 2 + 9, number_of_class], x_train)
    wnn.SGD(training_data, epochs, mini_batch_size, learning_rate, step)
    y_score, y_predict = wnn.predict(x_test, wnn)

    # 训练BP神经网络分类器
    # bpnn = BackPropagationNeuralNet([node_size, node_size * 2 + 9, number_of_class])
    # bpnn.SGD(training_data, epochs, mini_batch_size, learning_rate, step)
    # y_score, y_predict = bpnn.predict(x_test, bpnn)

    # 训练支持向量机
    # svm = svm.SVC()
    # svm.fit(x_train, y_train)
    # y_predict = svm.predict(x_test)

    # 训练线性判别器
    # lda = LinearDiscriminantAnalysis(solver='lsqr',shrinkage=None)
    # lda.fit(x_train, y_train)
    # y_predict = lda.predict(x_test)

    # 训练k近邻分类器
    # knn = KNeighborsClassifier(n_neighbors=1)
    # knn.fit(x_train, y_train)
    # y_predict = knn.predict(x_test)

    # 打印评估结果
    print("Cohen_kappa_score is:{0:.2f}".format(np.round(cohen_kappa_score(y_test, y_predict), 4)))
    print("Accuracy_score is:{0:.2f}%".format(np.round(accuracy_score(y_test, y_predict) * 100, 4)))
    print("F1_score is:{0:.2f}".format(np.round(f1_score(y_test, y_predict, average='weighted'), 4)))
    print("Precision_score is:{0:.2f}".format(np.round(precision_score(y_test, y_predict, average='weighted'), 4)))

    # # 绘制二位混淆矩阵
    matrix = confusion_matrix(y_test, y_predict, normalize='true')
    print(matrix.round(2))
    # name_of_class = ['EF', 'EE', 'FP', 'FS', 'HO', 'HC']
    # data = pd.DataFrame(matrix.round(2), index=name_of_class, columns=name_of_class)
    #
    # fig, ax = plt.subplots(figsize=(16, 16))
    # confusion = sns.heatmap(data, square=True, annot=True, cbar=False, cmap='Greens')
    # cb = confusion.figure.colorbar(confusion.collections[0])
    # ax.set_xlabel('Predicted')
    # ax.set_ylabel('True')
    # plt.title("归一化二维混淆矩阵")
    # plt.savefig("二维混淆矩阵.png")
    #
    # # 绘制绘制ROC
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    y_test = one_hot(y_test, number_of_class)
    for i in range(number_of_class):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    colors = cycle(['saddlebrown', 'red', 'green', 'royalblue', 'orangered', 'dodgerblue'])
    label = ['EF', 'EE', 'FP', 'FS', 'HO', 'HC']
    fig, ax = plt.subplots(figsize=(10, 8))
    for i, color in zip(range(number_of_class), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=2, label='{0} (area = {1:.2f})'.format(label[i], roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    ax.tick_params(labelsize=20)
    ax.set_xlabel('True positive rate', fontsize=20)
    ax.set_ylabel('False positive rate', fontsize=20)
    ax.legend(fontsize=20, loc='lower right')
    plt.title("ROC曲线", fontsize=30)
    plt.savefig("ROC曲线.png")
    #
    np.savetxt("fpr1.txt", fpr[0], delimiter="	")
    np.savetxt("fpr2.txt", fpr[1], delimiter="	")
    np.savetxt("fpr3.txt", fpr[2], delimiter="	")
    np.savetxt("fpr4.txt", fpr[3], delimiter="	")
    np.savetxt("fpr5.txt", fpr[4], delimiter="	")
    np.savetxt("fpr6.txt", fpr[5], delimiter="	")
    #
    np.savetxt("tpr1.txt", tpr[0], delimiter="	")
    np.savetxt("tpr2.txt", tpr[1], delimiter="	")
    np.savetxt("tpr3.txt", tpr[2], delimiter="	")
    np.savetxt("tpr4.txt", tpr[3], delimiter="	")
    np.savetxt("tpr5.txt", tpr[4], delimiter="	")
    np.savetxt("tpr6.txt", tpr[5], delimiter="	")
    # print(fpr)
    # print('\n')
    # print(tpr)
    #
    # # 绘制PRC曲线
    recall = dict()
    precision = dict()
    average_precision = dict()

    for i in range(number_of_class):
        precision[i], recall[i], _ = precision_recall_curve(y_test[:, i], y_score[:, i])
        average_precision[i] = average_precision_score(y_test[:, i], y_score[:, i])

    colors = cycle(['saddlebrown', 'red', 'green', 'royalblue', 'orangered', 'dodgerblue'])
    label = ['EF', 'EE', 'FP', 'FS', 'HO', 'HC']
    fig, ax = plt.subplots(figsize=(10, 8))
    for i, color in zip(range(number_of_class), colors):
        plt.plot(recall[i], precision[i], color=color, lw=2, label='{0} (area = {1:.2f})'.format(label[i], average_precision[i]))
    plt.plot([0, 1], [1, 0], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    ax.tick_params(labelsize=20)
    ax.set_xlabel('Recall', fontsize=20)
    ax.set_ylabel('Precision', fontsize=20)
    ax.legend(fontsize=20, loc='lower left')
    plt.title("PRC曲线", fontsize=30)
    plt.savefig("PRC曲线.png")
    #
    np.savetxt("recall1.txt", recall[0], delimiter="	")
    np.savetxt("recall2.txt", recall[1], delimiter="	")
    np.savetxt("recall3.txt", recall[2], delimiter="	")
    np.savetxt("recall4.txt", recall[3], delimiter="	")
    np.savetxt("recall5.txt", recall[4], delimiter="	")
    np.savetxt("recall6.txt", recall[5], delimiter="	")

    np.savetxt("precision1.txt", precision[0], delimiter="	")
    np.savetxt("precision2.txt", precision[1], delimiter="	")
    np.savetxt("precision3.txt", precision[2], delimiter="	")
    np.savetxt("precision4.txt", precision[3], delimiter="	")
    np.savetxt("precision5.txt", precision[4], delimiter="	")
    np.savetxt("precision6.txt", precision[5], delimiter="	")
    # print(recall)
    # print('\n')
    # print(precision)

    # # 10-折交叉验证
    # i = 1
    # F1 = []
    # KP = []
    # PR = []
    # ACC = []
    # step = 1
    # epochs = 256
    # mini_batch_size = 16
    # learning_rate = 1e-1
    # # x = np.loadtxt('x15.txt')
    # # y = np.loadtxt('y15.txt')
    # # svm = svm.SVC(kernel='linear')
    # svm = svm.SVC()
    # # knn = knn(n_neighbors=50)
    # # lda = lda(solver='lsqr', shrinkage=None)
    # cv = ShuffleSplit(n_splits=10)
    # for train_idx, test_idx in cv.split(y):
    #     x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)
    #
    #     # 特征标准化
    #     scaler = StandardScaler()
    #     x_train = scaler.fit_transform(x_train)
    #     x_test = scaler.transform(x_test)
    #
    #     # 特征选择
    #     pca = PCA(n_components=0.99)
    #     x_train = pca.fit_transform(x_train)
    #     x_test = pca.transform(x_test)
    #     training_data, testing_data = (x_train, y_train), (x_test, y_test)
    #
    #     node_size = x_train.shape[1]
    #
    #     # 训练小波神经网络分类器
    #     # wnn = wnn([node_size, node_size * 2 + 9, number_of_class], x_train)
    #     # wnn.SGD(training_data, epochs, mini_batch_size, learning_rate, step)
    #     # y_score, y_predict = wnn.predict(x_test, wnn)
    #
    #     # 训练BP神经网络分类器
    #     # bpnn = BackPropagationNeuralNet([node_size, node_size * 2 + 9, number_of_class])
    #     # bpnn.SGD(training_data, epochs, mini_batch_size, learning_rate, step)
    #     # y_score, y_predict = bpnn.predict(x_test, bpnn)
    #
    #     # 训练支持向量机
    #     svm.fit(x_train, y_train)
    #     y_predict = svm.predict(x_test)
    #
    #     # 训练线性判别器
    #     # lda.fit(x_train, y_train)
    #     # y_predict = lda.predict(x_test)
    #
    #     # 训练k近邻分类器
    #     # knn.fit(x_train, y_train)
    #     # y_predict = knn.predict(x_test)
    #
    #     KP.append(np.round(cohen_kappa_score(y_test, y_predict), 4))
    #     ACC.append(np.round(accuracy_score(y_test, y_predict) * 100, 4))
    #     F1.append(np.round(f1_score(y_test, y_predict, average='weighted'), 4))
    #     PR.append(np.round(precision_score(y_test, y_predict, average='weighted'), 4))
    #
    #     print("{}-f cross validation ".format(i))
    #     i = i + 1
    #
    # print("Ave kappa_value:({0:.2f} ± {1:.2f})".format(np.mean(KP), np.std(KP)))
    # print("Ave accuracy:({0:.2f} ± {1:.2f})".format(np.mean(ACC), np.std(ACC)))
    # # print("Ave f1_score:({0:.2f} ± {1:.2f})".format(np.mean(F1), np.std(F1)))
    # print("Ave precision:({0:.2f} ± {1:.2f})".format(np.mean(PR), np.std(PR)))
